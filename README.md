# Interview

Create a web page. You can use plain javascript, reactJs, vueJs or whichever framework/library you prefer to make it look similar to the low fidelity wireframes below.

Aspects to consider:
- Use media queries to make it responsive (desktop and mobile)
- A *chat* button always at the bottom right corner (available at [chat.svg](images/chat.svg))
- When clicking on *chat* button, the chat area is shown, when clicking again, the area is collapsed
- The chat has a text area and an input text. Whatever is typed in the input text can be sent by clicking on the *send* button (available at [send.svg](images/send.svg))
- The text must be an http POST request to https://interview-rubiko.free.beeceptor.com, which will return a 200 OK with a JSON 
  ```
  {
    "result": "Response from endpoint"
  }
  ``` 
![instructions](instructions.png)